/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import com.saigonbpo.ocr.https.client.SGBOCRHttpsClient;
import com.saigonbpo.ocr.https.client.SGBOCRHttpsClientRequest;
import com.saigonbpo.ocr.https.utils.SGBOCRHttpsClientUtils;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author saigonbpo
 */
public class SGBOCRHttpsClientExample {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        SGBOCRHttpsClientExample example = new SGBOCRHttpsClientExample();
        File file = new File("your file!!!");
        String result = example.doOCRWithFile(file);
        System.out.println(result);
    }

    public String doOCRWithFile(File file) throws IOException {
        String base64 = SGBOCRHttpsClientUtils.encodeToString(file);
        SGBOCRHttpsClientRequest request = new SGBOCRHttpsClientRequest();
        request.setImageBase64(base64);
        request.setImageType(SGBOCRHttpsClientRequest.IMAGE_TYPE.TIFF);
        request.setLanguage(SGBOCRHttpsClientRequest.LANGUAGE.VIE);
        request.setServiceType(SGBOCRHttpsClientRequest.SERVICE_TYPE.QUALITY_GRAY);
        request.setSourceType(SGBOCRHttpsClientRequest.SOURCE_TYPE.FILE);
        request.setOutputType(SGBOCRHttpsClientRequest.OUTPUT_TYPE.PLAIN_TEXT);
        request.setImagePreProcessing(true);
        request.setAutoRotation(true);
        request.setPriority((int) (Math.random() * 9));
        request.setReceiveTimout(0);

        String result = SGBOCRHttpsClient.getInstance().doOCR("https://10.1.15.100:58091/api/doOCR", request);
        return result;
    }

    private String doOCRWithBufferedImage(BufferedImage buff) throws IOException {
        String base64 = SGBOCRHttpsClientUtils.encodeToString(buff);
        SGBOCRHttpsClientRequest request = new SGBOCRHttpsClientRequest();
        request.setImageBase64(base64);
        request.setImageType(SGBOCRHttpsClientRequest.IMAGE_TYPE.JPG);
        request.setLanguage(SGBOCRHttpsClientRequest.LANGUAGE.ENG);
        request.setSourceType(SGBOCRHttpsClientRequest.SOURCE_TYPE.BUFFERED_IMAGE);
        request.setOutputType(SGBOCRHttpsClientRequest.OUTPUT_TYPE.PLAIN_TEXT);
        request.setImagePreProcessing(true);
        request.setPriority(9);

        String result = SGBOCRHttpsClient.getInstance().doOCR("https://10.1.15.100:58091/api/doOCR", request);
        return result;
    }

}